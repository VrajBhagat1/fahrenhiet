package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FahrenhietTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testconvertFromCelsius() {
		int f = Fahrenhiet.convertFromCelsius(20);
		assertTrue("The tempearture was not calculated properly", f == 68);
	}
	
	@Test
	public void testConvertFromCelsiusNegative() {
	int f = Fahrenhiet.convertFromCelsius(21);
	assertFalse("The tempearture was not calculated properly" , f == 68);
	}
	
	@Test
	public void testConvertFromCelsiusBoundryIn() {
	int f = Fahrenhiet.convertFromCelsius(32);
	assertTrue("The tempearture was not calculated properly", f == 89);
	}
	
	@Test
	public void testConvertFromCelsiusBoundryOut() {
	int f = Fahrenhiet.convertFromCelsius(32);
	assertFalse("The tempearture was not calculated properly" , f == 89.6);
	}
	
	
	
	

}
